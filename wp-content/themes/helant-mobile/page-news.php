<?php 
/**
* Template Name: Modèle de page actualités
**/
get_header(); ?>

<section id="content">

	<article id="page_main" <?php post_class(); ?>>
		<header id="page_header">
			<h1 id="page_title">
				<?php the_title(); ?>
			</h1>
		</header>
		
		<section id="page_content">
			<div id="page_content_the_content">
			
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
				
				<div id="webline_strap_news_sliderarea_block_content">
					<ul>
						<?php
						$lastpost = get_posts('showposts=30&orderby=date&order=desc');
						foreach($lastpost as $post) :
						setup_postdata($post);
						?>	
						<li>
							<span class="news_title">
								<?php the_title(); ?>
							</span>
							<?php the_excerpt(); ?>
						</li>
						<br/>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</section>
		
		<?php get_template_part( 'helant_page_menu_metiers' ); ?>
		
	</article>

</section>

<?php get_footer(); ?>