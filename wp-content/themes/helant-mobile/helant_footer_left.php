﻿<!--helant_footer_left.php-->

<div id="footer_left">
	<span id="copyright">
		<?php _e('© Copyright 2017 - INOTEAM','pure'); ?>
	</span>
	
	<nav id="navbottom">
		<?php wp_nav_menu( array('menu' => 'footer' )); ?>
	</nav>

</div>