<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title(' | ', true, 'right'); ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<?php wp_head(); ?>

<!--html5-->
<!--[if lte IE 8]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500,400italic,500italic,700,700italic' rel='stylesheet' type='text/css'>

<!--jQuery-->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<!--analytics-->
<script type="text/javascript">
(function (i, s, o, g, r, a, m) {i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {(i[r].q = i[r].q || []).push(arguments)}, i[r].l = 1 * new Date(); a = s.createElement(o),m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga'); ga('create', 'UA-53174801-1', 'auto');ga('send', 'pageview');
</script>

</head>
<body <?php body_class(); ?>>
<div id="main">
	<header id="header">
		<?php if( is_home()): ?>
			<h1 id="logo">
				<a href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo( 'name' ) ?>" rel="<?php bloginfo( 'name' ) ?>">
					<img src="<?php bloginfo('template_directory'); ?>/helant_img/logo.png" alt="<?php bloginfo( 'name' ) ?>" />
				</a>
			</h1>
		<?php else:?>
			<div id="logo">
				<a href="<?php echo home_url( '/' ); ?>" title="<?php bloginfo( 'name' ) ?>" rel="<?php bloginfo( 'name' ) ?>">
					<img src="<?php bloginfo('template_directory'); ?>/helant_img/logo.png" alt="<?php bloginfo( 'name' ) ?>" />
				</a>
			</div>
		<?php endif;?>	
		<div id="navtop_bt"></div>
		<nav id="navtop">
			<?php wp_nav_menu( array('menu' => 'header' )); ?>
		</nav>
	</header>
	<section id="container">