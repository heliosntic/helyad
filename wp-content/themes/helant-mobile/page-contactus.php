<?php 
/**
* Template Name: Modèle de page contactez-nous
**/
get_header(); ?>

<section id="content">

	<article id="page_main" <?php post_class(); ?>>
		<header id="page_header">
			<h1 id="page_title">
				<?php the_title(); ?>
			</h1>
		</header>
		
		<section id="page_content">
			
			<?php get_template_part( 'helant_home_agency' ); ?>
			
			<div id="page_content_the_content">

				<?php echo do_shortcode( '[contact-form-7 id="85" title="contactez-nous"]' ); ?>
				
			</div>

		</section>
		
		<?php get_template_part( 'helant_page_menu_metiers' ); ?>
		
	</article>

</section>

<?php get_footer(); ?>