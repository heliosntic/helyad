<?php 
/**
* Template Name: Modèle de page références
**/
get_header(); ?>

<section id="content">

	<article id="page_main" <?php post_class(); ?>>
		<header id="page_header">
			<h1 id="page_title">
				<?php the_title(); ?>
			</h1>
		</header>
		
		<section id="page_content">
			<div id="page_content_the_content">
			
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
					<br/>
				<?php endwhile; endif; ?>
					
				
				<p class="clear">
					<a class="red_link_double_phone" href="/?page_id=19">
						<?php _e('Vous souhaitez en savoir plus sur les projets menés','pure'); ?><br/>
						<?php _e('pour nos clients ? ','pure'); ?><strong><?php _e('Demandez un rendez-vous téléphonique','pure'); ?></strong>
					</a>
				</p>
				
			</div>
		</section>
		
		<?php get_template_part( 'helant_page_menu_metiers' ); ?>

	</article>

</section>

<?php get_footer(); ?>