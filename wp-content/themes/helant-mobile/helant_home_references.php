<!--helant_home_references.php-->

<aside id="references_block">
	<div class="home_title_style">
		<span><?php _e('de belles entreprises','pure'); ?></span><br/>
		<strong><?php _e('nous font confiance','pure'); ?></strong>
	</div>
	
	<?php get_template_part( 'helant_ref_list' ); ?>
	
	<div id="references_block_link">
		<a href="<?php bloginfo('wpurl'); ?>/?page_id=15">
			<span>
				<?php _e('Toutes nos références','pure'); ?>
			</span>
		</a>
	</div>

</aside>