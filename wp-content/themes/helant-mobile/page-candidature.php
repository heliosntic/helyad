<?php 
/**
* Template Name: Modèle de page candidatures spontanées
**/
get_header(); ?>

<section id="content">

	<article id="page_main" <?php post_class(); ?>>
		<header id="page_header">
			<h1 id="page_title">
				<?php the_title(); ?>
			</h1>
		</header>
		
		<section id="page_content">

			<div id="page_content_the_content">
				
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
				
				<?php echo do_shortcode( '[contact-form-7 id="86" title="recrutement"]' ); ?>
				
			</div>

		</section>
		
		<?php get_template_part( 'helant_page_menu_metiers' ); ?>
		
	</article>

</section>

<?php get_footer(); ?>