<?php

//the excerpt
function custom_excerpt_length( $length ) {
	return 14;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function new_excerpt_more($more) {
    global $post;
	return '... <br/><a class="news_content_link" href="'. get_permalink($post->ID) . '">Lire la suite</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

remove_filter( 'the_excerpt', 'wpautop' );

?>