<?php

add_theme_support( 'post-thumbnails' );
if ( function_exists( 'add_theme_support' ) ) {

};

// image page
if (class_exists('MultiPostThumbnails')) {
	new MultiPostThumbnails(array( 'label' => 'Image de gauche', 'id' => 'image_left', 'post_type' => 'page' ));
	new MultiPostThumbnails(array( 'label' => 'Decalco bas gauche', 'id' => 'image_left_bottom', 'post_type' => 'page' ));
};

?>