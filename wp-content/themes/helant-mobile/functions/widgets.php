<?php
//widget
function pure_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Accueil webline' ),
		'id' => 'widgetarea_webline',
		'before_widget' => '<div class="widgetarea_webline_content">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title">',
		'after_title' => '</div>',
	) );
	register_sidebar( array(
		'name' => __( 'Accueil contenu' ),
		'id' => 'widgetarea_home',
		'before_widget' => '<div class="widgetarea_home_content">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title">',
		'after_title' => '</div>',
	) );
	register_sidebar( array(
		'name' => __( 'Agences' ),
		'id' => 'widgetarea_agency',
		'before_widget' => '<div class="widgetarea_agency_content">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget_title">',
		'after_title' => '</div>',
	) );
	
}
add_action( 'widgets_init', 'pure_widgets_init' );

add_filter( 'widget_title', 'html_in_widget_title' );
function html_in_widget_title( $title ) {
	$title = str_replace( '[', '<', $title );
	$title = str_replace( '[/', '</', $title );
	$title = str_replace( ']', '>', $title );
	return $title;
}

?>