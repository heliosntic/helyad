<!--helant_footer_right.php-->

<div id="footer_right">
    <a href="http://www.groupehelios.fr/" id="helios" target="_blank"></a>
	<a id="footer_right_agency_link" href="<?php bloginfo('wpurl'); ?>/?page_id=19">
		<span>
			<?php _e('Nos implantations','pure'); ?>
		</span>
	</a>
	<a id="footer_right_fb_link" class="footer_right_social_link" href="#" STYLE="DISPLAY:NONE;">
		<?php _e('facebook','pure'); ?>
	</a>
	<a id="footer_right_linkedin_link" class="footer_right_social_link" href="#" STYLE="DISPLAY:NONE;">
		<?php _e('Linkedin','pure'); ?>
	</a>
	<a id="footer_right_viadeo_link" class="footer_right_social_link" href="#" STYLE="DISPLAY:NONE;">
		<?php _e('Viadeo','pure'); ?>
	</a>
</div>