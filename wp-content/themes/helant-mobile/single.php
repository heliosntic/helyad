<?php get_header(); ?>

<section id="content">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<article id="page_main" <?php post_class(); ?>>
		<header id="page_header">
			<h1 id="page_title">
				<?php the_title(); ?>
			</h1>
		</header>
		
		<section id="page_content">
			<?php the_content(); ?>
		</section>
		
		<?php get_template_part( 'helant_page_menu_metiers' ); ?>
		
	</article>

<?php endwhile; endif; ?>

</section>

<?php get_footer(); ?>