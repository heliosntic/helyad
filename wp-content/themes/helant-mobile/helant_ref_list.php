<!--helant_ref_list.php-->

<?php if( is_home()): ?>
	<?php
	$varref = new WP_query(array(
		'post_type'=> 'references',
		'posts_per_page'=> 4,
		'orderby' => 'rand'
	));
	if ( $varref->have_posts() ) : while ( $varref->have_posts() ) : $varref->the_post(); 
	?>
		<div class="references_block_single">
			<?php if (get_post_meta($post->ID,'Lien vers site de la référence',true)) : ?>
				<a href="<?php echo (''.get_post_meta($post->ID,'Lien vers site de la référence',true).''); ?>" target="blank" >
			<?php endif; ?>
				<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
			<?php if (get_post_meta($post->ID,'Lien vers site de la référence',true)) : ?>
				</a>
			<?php endif; ?>
		</div>
	<?php endwhile; endif; wp_reset_query(); ?>
<?php else:?>	
	<?php
	$varref = new WP_query(array(
		'post_type'=> 'references',
		'posts_per_page'=> 100,
		'orderby' => 'date',
		'order' => 'asc'
	));
	if ( $varref->have_posts() ) : while ( $varref->have_posts() ) : $varref->the_post(); 
	?>
		<div class="references_block_single">
			<?php if (get_post_meta($post->ID,'Lien vers site de la référence',true)) : ?>
				<a href="<?php echo (''.get_post_meta($post->ID,'Lien vers site de la référence',true).''); ?>" target="blank" >
			<?php endif; ?>
				<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
			<?php if (get_post_meta($post->ID,'Lien vers site de la référence',true)) : ?>
				</a>
			<?php endif; ?>
		</div>
	<?php endwhile; endif; wp_reset_query(); ?>
<?php endif;?>