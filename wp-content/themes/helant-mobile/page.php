<?php get_header(); ?>

<section id="content">

	<article id="page_main" <?php post_class(); ?>>
		<header id="page_header">
			<h1 id="page_title">
				<?php the_title(); ?>
			</h1>
		</header>
		
		<section id="page_content">
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div id="page_content_the_content">
					<?php the_content(); ?>
				</div>
			<?php endwhile; endif; ?>
		
		</section>
		
		<?php get_template_part( 'helant_call' ); ?>
			
		<?php get_template_part( 'helant_page_menu_metiers' ); ?>

	</article>

</section>

<?php get_footer(); ?>