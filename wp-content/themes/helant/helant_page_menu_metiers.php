<!--helant_page_menu_metiers.php-->

<nav id="navpage" <?php if (MultiPostThumbnails::has_post_thumbnail('page', 'image_left_bottom')): ?>class="navpage_with_padding"<?php endif; ?>>
	
	<span id="nav_title">
		<?php _e('Découvrez nos métiers et expertises','pure'); ?>
	</span>
	
	<?php wp_nav_menu( array('menu' => 'metiers' )); ?>
	
</nav>
