<?php get_header(); ?>

<section id="content">

	<aside id="page_sidebar">
		<img src="<?php bloginfo('template_directory'); ?>/helant_img/left_basic.jpg" />
	</aside>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<article id="page_main" <?php post_class(); ?>>
		<header id="page_header">
			<h1 id="page_title">
				<?php the_title(); ?>
			</h1>
		</header>
		
		<?php get_template_part( 'helant_breadcrumb' ); ?>
		
		<section id="page_content">
			<?php the_content(); ?>
		</section>
	</article>

<?php endwhile; endif; ?>

</section>

<?php get_footer(); ?>