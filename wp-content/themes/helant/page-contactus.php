<?php 
/**
* Template Name: Modèle de page contactez-nous
**/
get_header(); ?>

<section id="content">

	<aside id="page_sidebar">
		<?php if (MultiPostThumbnails::has_post_thumbnail('page', 'image_left')) : MultiPostThumbnails::the_post_thumbnail('page', 'image_left', NULL, ''); endif; ?>
	</aside>
	<article id="page_main" <?php post_class(); ?>>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
			<header id="page_header" style="background-image: url('<?php echo $image[0]; ?>')">
				<h1 id="page_title">
					<?php the_title(); ?>
				</h1>
			</header>
		<?php endif; ?>
		
		<?php get_template_part( 'helant_breadcrumb' ); ?>

        <iframe src="https://www.google.com/maps/d/embed?mid=1JYpxcIx5BmsltytcA1bBqjHPRDM" width="640" height="480" style="padding-left: 40px;"></iframe>
		<section id="page_content" class="page_content_large">
			
			<?php get_template_part( 'helant_home_agency' ); ?>
			
			<div id="page_content_the_content">

				<?php echo do_shortcode( '[contact-form-7 id="85" title="contactez-nous"]' ); ?>
				
			</div>

			<?php if (get_post_meta($post->ID,'Menu métiers ?',true)) : ?>
				<?php get_template_part( 'helant_page_menu_metiers' ); ?>
			<?php endif; ?>
			
		</section>
	</article>
	
	<?php if (MultiPostThumbnails::has_post_thumbnail('page', 'image_left_bottom')): ?>
		<div id="page_decalco_block">
			<?php MultiPostThumbnails::the_post_thumbnail('page', 'image_left_bottom', NULL, ''); ?>
		</div>
	<?php endif; ?>

</section>

<?php get_footer(); ?>