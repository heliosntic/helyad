<?php get_header(); ?>

<section id="content">
	
	<?php get_template_part( 'helant_webline' ); ?>
	
	<?php get_template_part( 'helant_home_metiers' ); ?>
	
	<?php get_template_part( 'helant_home_content' ); ?>

</section>

<?php get_footer(); ?>