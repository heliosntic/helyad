<!--helant_breadcrumb.php-->

<div id="breadcrumb">
	<div id="breadcrumb_content">
		<span>
			<a id="home" href="<?php echo home_url( '/' ); ?>">
				<?php _e( 'Accueil', 'pure' ); ?>
			</a>
		</span>
		<?php if (is_single()): ?> 
			<span> 
				<a href="<?php bloginfo('wpurl'); ?>/?page_id=57">
					<?php _e( 'Actualités', 'pure' ); ?>
				</a>
			</span>
			<span> 
				<?php the_title(); ?>
			</span>
		<?php elseif (is_page( 27 )) :?>
			<span> 
				<?php _e( 'Métiers et expertises ', 'pure' ); ?>
			</span>
			<span> 
				<?php _e( '> Distribution', 'pure' ); ?>
			</span>
		<?php elseif (is_page( 32 )) :?>
			<span> 
				<?php _e( 'Métiers et expertises ', 'pure' ); ?>
			</span>
			<span> 
				<?php _e( '> Intégration', 'pure' ); ?>
			</span>
		<?php elseif (is_page( 36 )) :?>
			<span> 
				<?php _e( 'Métiers et expertises ', 'pure' ); ?>
			</span>
			<span> 
				<?php _e( '> Tiers maintenance', 'pure' ); ?>
			</span>
		<?php else: ?>
			<span> 
				<?php the_title(); ?>
			</span>
		<?php endif; ?>
	</div>
</div>
