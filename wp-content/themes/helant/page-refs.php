<?php 
/**
* Template Name: Modèle de page références
**/
get_header(); ?>

<section id="content">

	<aside id="page_sidebar">
		<?php if (MultiPostThumbnails::has_post_thumbnail('page', 'image_left')) : MultiPostThumbnails::the_post_thumbnail('page', 'image_left', NULL, ''); endif; ?>
	</aside>
	<article id="page_main" <?php post_class(); ?>>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
			<header id="page_header" style="background-image: url('<?php echo $image[0]; ?>')">
				<h1 id="page_title">
					<?php the_title(); ?>
				</h1>
			</header>
		<?php endif; ?>
		
		<?php get_template_part( 'helant_breadcrumb' ); ?>
		
		<section id="page_content">
			<div id="page_content_the_content">
			
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
					<br/>
				<?php endwhile; endif; ?>
				
				<p class="clear">
					<a class="red_link_double_phone" href="/?page_id=19">
						<?php _e('Vous souhaitez en savoir plus sur les projets menés','pure'); ?><br/>
						<?php _e('pour nos clients ? ','pure'); ?><strong><?php _e('Demandez un rendez-vous téléphonique','pure'); ?></strong>
					</a>
				</p>
				
			</div>
			
			<?php if (get_post_meta($post->ID,'Menu métiers ?',true)) : ?>
				<?php get_template_part( 'helant_page_menu_metiers' ); ?>
			<?php endif; ?>
			
		</section>
	</article>

</section>

<?php get_footer(); ?>