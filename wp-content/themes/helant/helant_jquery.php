<!--helant_jquery.php-->

<!--sub-->
<script type="text/javascript">
	jQuery(function() {
		jQuery("#navtop ul li").click(function(){
			jQuery(this).addClass("this_hover").mouseleave(function(){
				jQuery(this).removeClass("this_hover");
			});
		});
	});
</script>

<!--news slider-->
<?php if( is_home()): ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/helant_js/slider.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){	
		var sudoSlider1 = jQuery("#webline_strap_news_sliderarea_block_content").sudoSlider({
			prevNext: true,
			auto: false
		});
	});	
</script>
<?php endif;?>