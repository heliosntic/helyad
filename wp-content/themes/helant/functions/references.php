<?php
//custom post articles references
add_action( 'init', 'cpt_references' );
function cpt_references(){
	$labels_references = array(
      'name' => 'Références',
      'all_items' => 'Toutes les références',
      'add_new' => 'Ajouter une référence',
      'add_new_item' => 'Ajouter un nouvelle référence',
      'edit_item' => 'Editer une référence',
      'new_item' => 'Nouvelle référence',
      'view_item' => 'Voir la fiche de la référence',
      'search_items' => 'Rechercher une référence',
      'not_found' =>  'Aucune référence',
      'not_found_in_trash' => 'Aucune référence',
      'menu_name' => 'Références'
    );
	register_post_type('references', array(
		'label' => __('Références'),
		'labels' => $labels_references,
		'singular_label' => __('Référence'),
		'public' => true,
		'show_ui' => true,
		'menu_icon' => get_bloginfo('template_directory') . '/helant_img/pic.png',
		'hierarchical' => false,
		'supports' => array('title', 'thumbnail', 'custom-fields'),
		'has_archive' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'menu_position' => 5
	));
}

//box champs perso
add_action('wp_insert_post', 'wpc_cpt_references');
function wpc_cpt_references($post_id){
	if ( $_GET['post_type'] == 'references' ) {
		add_post_meta($post_id, 'Lien vers site de la référence', '', true);
	}
	return true;
}

?>
