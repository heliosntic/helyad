<?php get_header(); ?>

<section id="content">

	<aside id="page_sidebar">
		<?php if (MultiPostThumbnails::has_post_thumbnail('page', 'image_left')) : MultiPostThumbnails::the_post_thumbnail('page', 'image_left', NULL, ''); endif; ?>
	</aside>
	<article id="page_main" <?php post_class(); ?>>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
			<header id="page_header" style="background-image: url('<?php echo $image[0]; ?>')">
				<h1 id="page_title">
					<?php the_title(); ?>
				</h1>
			</header>
		<?php endif; ?>
		
		<?php get_template_part( 'helant_breadcrumb' ); ?>
		
		<section id="page_content">
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div id="page_content_the_content">
					<?php the_content(); ?>
				</div>
			<?php endwhile; endif; ?>
			
			<?php if (get_post_meta($post->ID,'Menu métiers ?',true)) : ?>
				<?php get_template_part( 'helant_page_menu_metiers' ); ?>
			<?php endif; ?>
			
		</section>
	</article>
	
	<?php if (MultiPostThumbnails::has_post_thumbnail('page', 'image_left_bottom')): ?>
		<div id="page_decalco_block">
			<?php MultiPostThumbnails::the_post_thumbnail('page', 'image_left_bottom', NULL, ''); ?>
		</div>
	<?php endif; ?>

</section>

<?php get_footer(); ?>