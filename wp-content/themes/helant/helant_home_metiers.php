<!--helant_home_metiers.php-->
<aside id="home_metiers_strap">
	<a id="home_metiers_strap_green" href="<?php bloginfo('wpurl'); ?>/prescription-gmao-eam/">
	<span><?php _e('Inoteam à vos côtés pour la réussite de votre projet GMAO.','pure'); ?><br/><strong><?php _e('En savoir +','pure'); ?></strong></span>
	</a>
	<a id="home_metiers_strap_orange" href="<?php bloginfo('wpurl'); ?>/integration-gmao-eam/">
	<span><?php _e('Nos consultants GMAO - EAM sont disponibles pour vous accompagner et vous conseiller.','pure'); ?><br/><strong><?php _e('En savoir +','pure'); ?></strong></span>
	</a>
	<a id="home_metiers_strap_blue" href="<?php bloginfo('wpurl'); ?>/distribution/">
	<span><?php _e('Nous disposons d\'une équipe commerciale et avant-vente à votre écoute.','pure'); ?><br/><strong><?php _e('En savoir +','pure'); ?></strong></span>
	</a>
</aside>