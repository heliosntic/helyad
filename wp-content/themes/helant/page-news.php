<?php 
/**
* Template Name: Modèle de page actualités
**/
get_header(); ?>

<section id="content">

	<aside id="page_sidebar">
		<?php if (MultiPostThumbnails::has_post_thumbnail('page', 'image_left')) : MultiPostThumbnails::the_post_thumbnail('page', 'image_left', NULL, ''); endif; ?>
	</aside>
	<article id="page_main" <?php post_class(); ?>>
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
			<header id="page_header" style="background-image: url('<?php echo $image[0]; ?>')">
				<h1 id="page_title">
					<?php the_title(); ?>
				</h1>
			</header>
		<?php endif; ?>
		
		<?php get_template_part( 'helant_breadcrumb' ); ?>
		
		<section id="page_content">
			<div id="page_content_the_content">
			
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
					<br/>
				<?php endwhile; endif; ?>
				
				<div id="webline_strap_news_sliderarea_block_content">
					<ul>
						<?php
						$lastpost = get_posts('showposts=30&orderby=date&order=desc');
						foreach($lastpost as $post) :
						setup_postdata($post);
						?>	
						<li>
							<span class="news_title">
								<?php the_title(); ?>
							</span>
							<?php the_excerpt(); ?>
						</li>
						<br/>
						<?php endforeach; ?>
					</ul>
				</div>

				
			</div>
			
			<?php if (get_post_meta($post->ID,'Menu métiers ?',true)) : ?>
				<?php get_template_part( 'helant_page_menu_metiers' ); ?>
			<?php endif; ?>
			
		</section>
	</article>

</section>

<?php get_footer(); ?>