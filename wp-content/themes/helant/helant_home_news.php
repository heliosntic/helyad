<!--helant_home_news.php-->

<aside id="webline_strap_news_sliderarea">
	<div id="webline_strap_news_sliderarea_block">
		<div id="webline_strap_news_sliderarea_block_content">
			<ul>
				<?php
				$lastpost = get_posts('showposts=4&orderby=date&order=desc');
				foreach($lastpost as $post) :
				setup_postdata($post);
				?>	
				<li>
					<span class="news_title">
						<?php the_title(); ?>
					</span>
					<?php the_excerpt(); ?>
					<a href="<?php bloginfo('wpurl'); ?>/actualites/">
						<?php _e('Voir toutes les actus','pure'); ?>
					</a>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</aside>