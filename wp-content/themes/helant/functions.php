<?php
add_action('after_setup_theme', 'pure_setup');
function pure_setup(){
	load_theme_textdomain('pure', get_template_directory() . '/languages');
	add_theme_support('automatic-feed-links');
	register_nav_menus(
	array( 'main-menu' => __( 'Main Menu', 'pure' ) )
	);
}

function pure_wp_title( $title, $sep ) {
	global $paged, $page;
	if ( is_feed() )
		return $title;
	$title .= get_bloginfo( 'name', 'display' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'pure' ), max( $paged, $page ) );
	return $title;
}
add_filter( 'wp_title', 'pure_wp_title', 10, 2 );

//box champs perso page
add_action('wp_insert_post', 'wpc_cpt_pages');
function wpc_cpt_pages($post_id){
	if ( $_GET['post_type'] == 'page' ) {
		add_post_meta($post_id, 'Menu métiers ?', '', true);
	}
	return true;
}


require_once(TEMPLATEPATH . '/functions/widgets.php');
require_once(TEMPLATEPATH . '/functions/the_excerpt.php');
require_once(TEMPLATEPATH . '/functions/references.php');
require_once(TEMPLATEPATH . '/functions/thumbnails.php');


?>